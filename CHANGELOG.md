# Changelog

## 1.0.0 (30 de Setembro de 2013)

* Iniciado o Readme. (Gio)
* Iniciado o Changelog. (Gio)
* Baixado e iniciado com o HTML5 Boilerplate 4.3.0, de 10 de Setembro de 2013. (Gio)
* Início do Framework da Weecom. (Gio)

## 1.1.0 (13 de Janeiro de 2014)

* Retirado o Browse Happy e inserido o Browser-Update.org (Gio)
* Atualizado o Readme, inserido o Semantic Versioning (Gio)
* Passadas a tabulação para tabs e não espaços (Gio)
* Traduzido os pedaços em inglês do index.html e inserida outras orientações (Gio)

## 1.1.1 (20 de Janeiro de 2014)

* Correção no link do Browser-Update.org (Gio)

## 1.1.2 (29 de Janeiro de 2014)

* Retirado o http do link do Browser-Update.org pra deixar o browser usar o protocolo ativo (Gio)

## 1.2.0 (30 de Janeiro de 2014)

* Criação da pasta "site-publico" e envolvimento dos arquivos ideiais para evitar que os mesmos fiquem soltos na raiz do FTP. (Gio)
* Feito um Humans TXT pra empresa (Gio)

## 1.3.0 (31 de Janeiro de 2014)

* Espaços convertidos para tabs. (Gio)

## 1.4.0 (3 de Fevereiro de 2014)

* Adicionado box-sizing border-box pra tudo. (Gio)

## 1.5.0 (11 de Fevereiro de 2014)

* Adicionado o lang="pt-br". (Gio)

## 1.6.0 (13 de Fevereiro de 2014)

* Adicionado o twitter do Airton no humans.txt. (Gio)

## 1.7.0 (17 de Fevereiro de 2014)

* Adicionado a classe hidden-if-js no main.css. (Gio)

## 1.8.0 (24 de Fevereiro de 2014)

* Adicionada uma marcação inicial com header, nav, main e footer. (Gio)

## 1.9.0 (2 de Abril de 2014)

* Modificada a regra de reescrita do .htaccess padrão para forçar com www e inserida a condição para ignorar nosso servidor local. (Gio)