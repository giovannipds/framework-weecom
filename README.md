# Framework Weecom

Bem-vindo ao Framework da Weecom.

O Framework é atualmente composto pelas seguintes bibliotecas e/ou características:

* [HTML5 Boilerplate](http://html5boilerplate.com/) 4.3.0
* [Browser-Update.org](http://browser-update.org/)
* [Semantic Versioning](http://semver.org/) 2.0.0

Tendo portado, portanto, as características dos mesmos, que podem ser conferidos no link oficial destes projetos.


## Antes de iniciar

Antes de iniciar um novo projeto com o framework, ajude-nos a mantê-lo atualizado, conferindo se alguma das bibliotecas utilizadas por ele, já está em uma nova versão, como por exemplo, o [HTML5 Boilerplate](http://html5boilerplate.com). Se estiver, você pode atualizar a versão atual do framework, seguindo as diretrizes da seção "Como contribuir", logo abaixo.


## Como contribuir

NADA no framework deve ser editado sem um motivo definido, sem ser documentado com o que foi feito, inserido, alterado ou retirado e por causa de quê. Toda e qualquer alteração que deverá ser feita, deve ser comunicada previamente a equipe, e somente após aprovada, deverá ser executada e discriminada no Changelog, alterando a versão do framework conforme o mesmo é evoluído.


### [Changelog](CHANGELOG.md)

### To-do

* Facebook OpenGraphs



---


# [HTML5 Boilerplate](http://html5boilerplate.com)

HTML5 Boilerplate is a professional front-end template for building fast,
robust, and adaptable web apps or sites.

This project is the product of many years of iterative development and combined
community knowledge. It does not impose a specific development philosophy or
framework, so you're free to architect your code in the way that you want.

* Source: [https://github.com/h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate)
* Homepage: [http://html5boilerplate.com](http://html5boilerplate.com)
* Twitter: [@h5bp](http://twitter.com/h5bp)


## Quick start

Choose one of the following options:

1. Download the latest stable release from
   [html5boilerplate.com](http://html5boilerplate.com/) or a custom build from
   [Initializr](http://www.initializr.com).
2. Clone the git repo — `git clone
   https://github.com/h5bp/html5-boilerplate.git` - and checkout the tagged
   release you'd like to use.


## Features

* HTML5 ready. Use the new elements with confidence.
* Cross-browser compatible (Chrome, Opera, Safari, Firefox 3.6+, IE6+).
* Designed with progressive enhancement in mind.
* Includes [Normalize.css](http://necolas.github.com/normalize.css/) for CSS
  normalizations and common bug fixes.
* The latest [jQuery](http://jquery.com/) via CDN, with a local fallback.
* The latest [Modernizr](http://modernizr.com/) build for feature detection.
* IE-specific classes for easier cross-browser control.
* Placeholder CSS Media Queries.
* Useful CSS helpers.
* Default print CSS, performance optimized.
* Protection against any stray `console.log` causing JavaScript errors in
  IE6/7.
* An optimized Google Analytics snippet.
* Apache server caching, compression, and other configuration defaults for
  Grade-A performance.
* Cross-domain Ajax and Flash.
* "Delete-key friendly." Easy to strip out parts you don't need.
* Extensive inline and accompanying documentation.


## Documentation

Take a look at the [documentation table of contents](doc/TOC.md). This
documentation is bundled with the project, which makes it readily available for
offline reading and provides a useful starting point for any documentation you
want to write about your project.